#!/bin/sh

set -e

echo "I: create user"
adduser --gecos User --disabled-login user

echo "I: set user password"
echo "user:user" | chpasswd

if [ -x /bin/bash ]; then
  echo "I: set user shell to /bin/bash"
  chsh --shell /bin/bash user
  echo "I: set root shell to /bin/bash"
  chsh --shell /bin/bash
elif [ -x /bin/ash ]; then
  echo "I: set user shell to /bin/ash"
  chsh --shell /bin/ash user
  echo "I: set root shell to /bin/ash"
  chsh --shell /bin/ash
fi
