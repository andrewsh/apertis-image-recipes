#!/bin/sh

set -e

echo "I: create sysroot directory on home partition"
install -d -m 0755 /home/sysroot
chown user.user /home/sysroot
