#!/bin/sh

set -e

TMPFILE=/usr/lib/tmpfiles.d/canterbury-platform-cache.conf
CACHE=apertis-platform.xml.gz
SOURCE=/var/cache/app-info/xmls
TARGET=/usr/share/canterbury

# Check the canterbury cache filed
if [ ! -f $SOURCE/$CACHE ] ; then
	# Exit successfully if Canterbury is not in the image
	exit 0
fi

# Move the canterbury cache file to /usr
mkdir -p $TARGET
mv $SOURCE/$CACHE $TARGET

# Create a symbolic link at the standard place
echo "L+ $SOURCE/$CACHE - - - - $TARGET/$CACHE" > $TMPFILE
